/* 
 * Copyright 2017 Giuseppe Fabiano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <algorithm>

using namespace std;

const int nullptr = 0;

struct Node {
    int value;
    Node *left;
    Node *right;

    explicit Node(int val): value(val), left(nullptr), right(nullptr){}
};


void Insert(Node *&root_, int val) {
    Node *newNode = new Node(val);

    Node *pre = nullptr;
    Node *post = root_;
    while (post != nullptr) {
        pre = post;
        if (val <= post->value)
            post = post->left;
        else
            post = post->right;
    }

    if (pre == nullptr)
        root_ = newNode;
    else if (val <= pre->value)
        pre->left = newNode;
    else
        pre->right = newNode;
}

int Height( Node *root_ ) {
    if (root_) {
        if (!root_->left && !root_->right)
            return 1;
    
        int left, right;
    
        left = Height(root_->left);
        right = Height(root_->right);

        if (left >= right)
            return left+1;
        else
            return right+1;
    
    }

    return 0;
}



int main() {
    int N, tmp;
    Node *root = nullptr;

    cin >> N;
    for ( int i = 0; i < N; ++i ) {
        cin >> tmp;
        Insert(root, tmp);
    }

    cout << Height(root) << endl;

    return 0;
}
