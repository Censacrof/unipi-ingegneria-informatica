/* 
 * Copyright 2017 Giuseppe Fabiano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

struct str {
    string stringa;
    int freq;
};

int FreqCmp(const str &str1, const str &str2) {
    return str1.freq > str2.freq;
}


int StrCmp(const str &str1, const str &str2) {
    return str1.stringa < str2.stringa;
}

int main() {
    int N, K;
    cin >> N >> K;

    if (K > N)
        return 1;

    vector<str> v;

    str temp = {"", 1};
    for (int i = 0; i < N; i++) {
        cin >> temp.stringa;

        bool found = false;
        for (int j = 0; j < v.size(); ++j) {
            if (v[j].stringa == temp.stringa) {
                v[j].freq++;
                found = true;
                break;
            }
        }

        if (!found)
            v.push_back(temp);
    }

    sort(v.begin(), v.end(), FreqCmp);       // O(nlogn)
    sort(v.begin(), v.begin() + K, StrCmp);  // O(KlogK)
    // Siccome K <= n allora la complessità sarà max O(2nlog(n)) = O(nlogn)

    for (int i = 0; i < K; i++) {
        cout << v[i].stringa << endl;
    }

    return 0;
}
