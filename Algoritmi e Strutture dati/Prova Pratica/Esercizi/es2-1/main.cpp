/* 
 * Copyright 2017 Giuseppe Fabiano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

bool IntDesc (int i, int j) { return (i > j); }

int main() {
    int N, pos;
    cin >> N;

    vector<int> v;
    pos = 0;
    for (int i= 0; i < N; ++i) {
        int temp;
        cin >> temp;
        if (temp % 2 == 0) {
            if (v.size() != 0) {
                v.push_back(v[pos]);
                v[pos] = temp;
            } else {
                v.push_back(temp);
            }
            pos++;
        } else {
            v.push_back(temp);
        }
    }

    // La complessità dei due sort:
    //      Caso migliore: O(nlog(n/2))
    //      Caso peggiore: O(nlogn)
    sort(v.begin(), v.begin() + pos);
    sort(v.begin() + pos, v.end(), IntDesc);

    for (vector<int>::iterator it = v.begin(); it != v.end(); ++it) {
        cout << *it << endl;
    }

    return 0;
}
