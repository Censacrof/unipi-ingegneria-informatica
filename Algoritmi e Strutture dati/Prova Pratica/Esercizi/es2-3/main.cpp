#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

struct Richiesta {
    int ID;
    int priority;
};

int cmp(const Richiesta& a, const Richiesta& b) {
    if ( a.ID == b.ID )
        return a.priority > b.priority;

    return a.ID < b.ID;
}

int main() {
    int N;
    vector<Richiesta> v;

    cin >> N;

    for (int i = 0; i < N; ++i) {
        Richiesta tmp;

        cin >> tmp.ID >> tmp.priority;

        v.push_back(tmp);
    }

    for (vector<Richiesta>::iterator it = v.begin(); it != v.end(); ++it) {
        cout << '<' << it->ID << ',' << it->priority << '>';
    }

    cout << endl;

    sort(v.begin(), v.end(), cmp);


    for (vector<Richiesta>::iterator it = v.begin(); it != v.end(); ++it) {
        cout << '<' << it->ID << ',' << it->priority << '>';
    }

    cout << endl;

    return 0;
}
