# UNIPI - Ingegneria Informatica #

![](http://i.imgur.com/xQ0byMV.png)

Questo è un repository con materiale utile allo studio di Ingegneria Informatica dell'università di Pisa

### A cosa serve questo repository? ###

Questo repository è stato ideato con l'intento di accumulare/creare del materiale utile allo studio di Ingegneria Informatica

### Come comincio? ###

Cominciare è facile! Basta andare nella sezione [source](https://bitbucket.org/gfabiano/unipi-ingegneria-informatica/src) per visionare il materiale.

### Come posso contribuire? ###

Puoi contribuire in vari modi:

* Se trovi errori o hai qualche consiglio/richiesta vai nel campo [issues](https://bitbucket.org/gfabiano/unipi-ingegneria-informatica/issues?status=new&status=open) e descrivi dettagliatamente cosa vuoi comunicarmi
* Condividi del materiale/appunti e verrai inserito tra i contributors del progetto :)

>Se vuoi condividere qualcosa fai una [pull request](https://bitbucket.org/gfabiano/unipi-ingegneria-informatica/pull-requests/). Se non riesci non esitare a mandami una mail gfabiano40@gmail.com

### Come posso ringraziarti? ###

Non ho grosse pretese, mi basta una stretta di mano, ma se vuoi offrirmi un caffè o una birra non rifiuto :D
